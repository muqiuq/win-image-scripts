$ScriptDir = Split-Path $script:MyInvocation.MyCommand.Path
Dism /Get-WimInfo /WimFile:$ScriptDir\Image\sources\install.wim
$indexToDelete = Read-Host "Enter index number to delete"
Dism /Delete-Image /ImageFile:$ScriptDir\Image\sources\install.wim /Index:$indexToDelete
Dism /Get-WimInfo /WimFile:$ScriptDir\Image\sources\install.wim