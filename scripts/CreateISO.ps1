$env:Path += ";C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Deployment Tools\amd64\Oscdimg"

$version = Read-Host "Version (without v at beginning)"
$ScriptDir = Split-Path $script:MyInvocation.MyCommand.Path
$ImgWorkDir = $ScriptDir
$ISOFilename = "Win11Ent_EN_V" + $version + ".iso"

oscdimg.exe -m -o -u2 -udfver102 -bootdata:2#p0,e,b$ImgWorkDir\Image\boot\etfsboot.com#pEF,e,b$ImgWorkDir\Image\efi\microsoft\boot\efisys.bin $ImgWorkDir\Image $ImgWorkDir\$ISOFilename

Write-Host "Done"