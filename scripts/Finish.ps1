$ScriptDir = Split-Path $script:MyInvocation.MyCommand.Path
$MountDir = Join-Path -Path $ScriptDir -ChildPath "Mount"

New-Item -ItemType Directory -Force -Path $MountDir\Windows\OEM\
New-Item -ItemType Directory -Force -Path $MountDir\Windows\ThirdPartyInstaller
New-Item -ItemType Directory -Force -Path $MountDir\Windows\Panther\

cp $ScriptDir\EI.CFG $ScriptDir\Image\sources
cp $ScriptDir\Windows11Ent.xml $MountDir\Windows\Panther\unattend.xml
cp $ScriptDir\Welcome.ps1 $MountDir\Windows\OEM\Welcome.ps1
copy -v $ScriptDir\ApplicationsToInstall\* $MountDir\Windows\ThirdPartyInstaller

Dism /Unmount-Image /MountDir:$MountDir /Commit

rm $MountDir

Dism /Cleanup-Wim

