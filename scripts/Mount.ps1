$ScriptDir = Split-Path $script:MyInvocation.MyCommand.Path
$MountDir = Join-Path -Path $ScriptDir -ChildPath "Mount"

New-Item -ItemType Directory -Force -Path $ScriptDir\ApplicationsToInstall

Write-Host "Creating mount dir"
mkdir $MountDir

Write-Host "Mounting Wim"
Dism /Mount-Wim /WimFile:$ScriptDir\Image\sources\install.wim /index:1 /MountDir:$MountDir