$ScriptDir = Split-Path $script:MyInvocation.MyCommand.Path
$MountDir = Join-Path -Path $ScriptDir -ChildPath "Mount"


$directories = @("ApplicationsToInstall", "Image")
foreach ($var in $directories) {
    $fullPath = Join-Path -Path $ScriptDir -ChildPath $var
    if (Test-Path $fullPath) { 
        Write-Host "Deleting $fullPath"
        Remove-Item -Path $fullPath -Recurse -Force
    }
}

Remove-Item -Path  -Recurse -Force

Dism /Unmount-Image /MountDir:$MountDir /Commit

rm $MountDir

Dism /Cleanup-Wim

Write-Host "Clean up complete"