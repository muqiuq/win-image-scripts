# https://github.com/JosephM101/Force-Windows-11-Install/blob/main/Win11-TPM-RegBypass.ps1
$WIMScratchDir = "C:\WinBuild\Enterprise11\MountBoot"

Write-Host "Mounting Boot.wim"
mkdir $WIMScratchDir

Dism /Mount-Wim /WimFile:C:\WinBuild\Enterprise11\Image\sources\boot.wim /index:2 /MountDir:$WIMScratchDir

Function CollectGarbage {
    Write-Host "Cleaning up..."
    [gc]::Collect(1000, [System.GCCollectionMode]::Forced , $true) # Clean up
    [gc]::WaitForPendingFinalizers() # Wait for cleanup process to finish
    Start-Sleep 1
} 

$ScriptDir = Split-Path $script:MyInvocation.MyCommand.Path


$REG_System = Join-Path $WIMScratchDir -ChildPath "\Windows\System32\config\system"
$VirtualRegistryPath_SYSTEM = "HKLM\WinPE_SYSTEM"
$VirtualRegistryPath_Setup = $VirtualRegistryPath_SYSTEM + "\Setup"
Write-Host "Unloading (Just in Case)..."
reg unload $VirtualRegistryPath_SYSTEM | Out-Null # Just in case...
Write-Host "Fixing registry..."
Start-Sleep 1
reg load $VirtualRegistryPath_SYSTEM $REG_System | Out-Null

Set-Location -Path Registry::$VirtualRegistryPath_Setup

New-Item -Name "LabConfig"
New-ItemProperty -Path "LabConfig" -Name "BypassTPMCheck" -Value 1 -PropertyType DWORD -Force
New-ItemProperty -Path "LabConfig" -Name "BypassSecureBootCheck" -Value 1 -PropertyType DWORD -Force
New-ItemProperty -Path "LabConfig" -Name "BypassRAMCheck" -Value 1 -PropertyType DWORD -Force
New-ItemProperty -Path "LabConfig" -Name "BypassStorageCheck" -Value 1 -PropertyType DWORD -Force
New-ItemProperty -Path "LabConfig" -Name "BypassCPUCheck" -Value 1 -PropertyType DWORD -Force

Set-Location -Path $ScriptDir
CollectGarbage
Start-Sleep 1
reg unload $VirtualRegistryPath_SYSTEM
Write-Host "Done Injecting"

Write-Host "Unmounting image"

Dism /Unmount-Image /MountDir:$WIMScratchDir /Commit

rm $WIMScratchDir

Dism /Cleanup-Wim

Write-Host "Done"