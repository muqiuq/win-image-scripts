$ScriptDir = Split-Path $script:MyInvocation.MyCommand.Path
New-Item -ItemType Directory -Force -Path $ScriptDir\Image

$firstIso = Get-ChildItem -Path $ScriptDir -Filter *.iso -File | Select-Object -First 1

# Check if an ISO file was found and output the full path
if ($firstIso) {
    $firstIso.FullName
} else {
    Write-Output "No ISO files found in the specified directory."
}

$img = Mount-DiskImage -ImagePath $firstIso.FullName

$volume = (Get-DiskImage -DevicePath $img.DevicePath | Get-Volume)

$sourcePath = $volume.DriveLetter + ":\*"

Copy-Item -Path $sourcePath  -Destination $ScriptDir\Image -Recurse -Force

Dismount-DiskImage -ImagePath $firstIso.FullName