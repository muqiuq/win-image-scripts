# Script collection for Windows Image Building

My personal collection of Scripts I use to customize Windows Images. 

I mostly use it to debloat Windows and to automate Windows Installation. 

# Workflow

**The scripts are not ready to use and need to be customized first.**

 - open PowerShell Administrator Console
 - Put ISO File into folder that contains all the scripts (here: `scripts`)
 - Create unattend xml file using [Windows System Image Manager](https://learn.microsoft.com/en-us/windows-hardware/customize/desktop/wsim/windows-system-image-manager-technical-reference). See examples in folder [unattend](unattend/)
 - `.\UnpackISO.ps1` to extract all files from the source ISO (Windows Installation ISO) to a directory
 - Use `.\DeleteImages.ps1` to remove all unwanted Windows Installation Variants 
 - Use `.\BypassWin11InstallCheck.ps1` to bypass installation checks of Windows 11
 - Use `.\Mount.ps1` to mount installation image (`install.wim`)
 - Make custom changes in mounted `install.wim`
 - Adjust `Welcome.ps1` script. This script will run after installation (see [unattend](unattend/) examples)
 - Add applications to install to `ApplicationsToInstall` and adjust `Welcome.ps1`
 - `.\Finish.ps1` to pack everything into `install.wim`
 - adjust filename in `.\CreateISO.ps1` and then execute it to create installation iso